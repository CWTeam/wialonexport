# Description

"WialonExport" is a NodeJS application that allows to export messages of units to Wialon messages files.

# Installation

- Install [NodeJS](http://nodejs.org/) (v4.2.1)
- Extract whole **WialonExport.zip** archive (keeping directory structure) to a folder

# Usage

    node main --help
    
    USAGE: node main [OPTION1] [OPTION2]... arg1 arg2...
    The following options are supported:
      -s, --site <ARG1>     URL of Wialon Site (i.e. https://hst-api.wialon.com) (mandatory)
      -k, --token  <ARG1>   Token of Wialon user
      -f, --from <ARG1>     From date/time in "YYYY-MM-DD HH:mm:ss" format (365 days back by default)
      -t, --to <ARG1>       To date/time (exclusive) in "YYYY-MM-DD HH:mm:ss" format (now by default)
      --format <ARG1>       Format (wlb|wln|plt|txt|kml) of Wialon files  ("wlb" by default)
      --folder <ARG1>       Output Folder for Wialon files
      --nocompression       No Compression of Wialon files
      --nomsgsok            No Messages is OK
      -u, --user <ARG1>     Name of Wialon User (legacy login method) 
      -p, --pass <ARG1>     Password of Wialon user (legacy login method)

## Examples

### Date range examples

- Export messages for **365 days** back from **now**

        node main -s https://hst-api.wialon.com -u b3test_en -p test
    
- Export messages for **March 2014**

        node main -s https://hst-api.wialon.com -u b3test_en -p test --from 2014-03 --to 2014-04

- Export messages for given date and time range (i.e. 1 day, 38 minutes and 21 seconds)

        node main -s https://hst-api.wialon.com -u b3test_en -p test --from "2014-03-01 13:00" --to "2014-03-02 13:38:21"

### File format examples

- Export messages in **wln** format (instead of default 'wlb')

        node main -s https://hst-api.wialon.com -u b3test_en -p test --format=wln
    
- Export messages in **txt** NMEA format **without compression**

        node main -s https://hst-api.wialon.com -u b3test_en -p test --format=txt --nocompression

### Miscellaneous

- Target export files to a different **destination folder** (instead of current folder)

        node main -s https://hst-api.wialon.com -u b3test_en -p test --folder "data/2014"
    
- Suppress **warnings** for units with **no messages**

        node main -s https://hst-api.wialon.com -u b3test_en -p test --nomsgsok

# Revision history

## 0.3.0 (Oct 28, 2015)

- Added support of the new authorization via token.

## 0.2.0 (Oct 10, 2014)

- Added description of expected date/time format.
- Added check that 'from' date/time is less than 'to' date/time.
- Added printing of date range in [from, to) to console.

## 0.1.0 (Oct 7, 2014)

- Initial version of the application.
